package ChessBoard;

public class ChessBoard {

	private static int n = 3;
	

	static int x = 0;
	static int y = 0;

	/**/

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		int[][] chbd = new int[3][3];

		x =  Integer.parseInt(args[0]);
		y = Integer.parseInt(args[1]);

		System.out.println(" Given Input (" + x + " " + y + ") ");

		moveTwoSteps(x, y);
	}

	public static void moveTwoStepsCorner(int x, int y) {

		if (x == n && y == 1) {
			x = x - 1;
			y = y;

			System.out.print(" (" + x + " " + y + ") ");

			// step 2
			x = x - 1;
			y = y;
			System.out.print(" (" + x + " " + y + ") ");

			moveOneStepY(x, y);

		} else if (y == n && x == 1) {
			y = y - 1;
			x = x;
			System.out.print(" (" + x + " " + y + ") ");

			y = y - 1;
			x = x;
			System.out.print(" (" + x + " " + y + ") ");

			moveOneStepX(x, y);
		} else if (x == 1 && y == 1) {
			y = y + 1;
			x = x;
			System.out.print(" (" + x + " " + y + ") ");

			y = y + 1;
			x = x;
			System.out.print(" (" + x + " " + y + ") ");

			moveOneStepX(x, y);

		}
	}

	public static void moveTwoSteps(int x, int y) {
		if (isThisCorner(x, y)) {
			moveTwoStepsCorner(x, y);
		} else {

			if (y == 3) {
				y = y - 1;
				x = x;
				System.out.print(" (" + x + " " + y + ") ");

				y = y - 1;
				x = x;
				System.out.print(" (" + x + " " + y + ") ");

				moveOneStepX(x, y);
			}else if (x > y) {
				y = y + 1;
				x = x;
				System.out.print(" (" + x + " " + y + ") ");

				y = y + 1;
				x = x;
				System.out.print(" (" + x + " " + y + ") ");

				moveOneStepX(x, y);
			}else if (y > x) {
                 x = x+1;
                 y = y;
                 System.out.print(" (" + x + " " + y + ") ");
                 
                 x = x+1;
                 y = y;
                 System.out.print(" (" + x + " " + y + ") ");;
                 
                 moveOneStepY(x, y);
			}

		}
	}

	public static void moveOneStepX(int x, int y) {
		x = x + 1;
		y = y;
		System.out.println(" (" + x + " " + y + ") ");

		if ((x == n) && (y == n)) {
			System.out.println("got the path 3 3");
		} else {
			System.out.println(" (" + x + " " + y + ") ");
			moveTwoSteps(x, y);
		}
	}

	public static void moveOneStepY(int x, int y) {
		x = x;
		y = y + 1;
		System.out.println(" (" + x + " " + y + ") ");

		if ((x == n) && (y == n)) {
			System.out.println("got the path 3 3");
		} else {
			System.out.println(" (" + x + " " + y + ") ");
			moveTwoSteps(x, y);
		}
	}

	public static boolean isThisCorner(int x, int y) {
		if (x == n || y == n) {
			if (((x - y) == (n - 1)) || ((y - x) == (n - 1)) || ((x - y) == 0))
				return true;
		} else if (x == 1 && y == 1) {
			return true;
		}

		return false;
	}

}
