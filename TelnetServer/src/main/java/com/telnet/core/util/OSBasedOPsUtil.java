package com.telnet.core.util;

import java.util.StringTokenizer;
import java.util.List;

/**
 * Created by IntelliJ IDEA.
 * User: nareshk
 * Date: Dec 20, 2012
 * Time: 6:52:51 PM
 * To change this template use File | Settings | File Templates.
 */
public class OSBasedOPsUtil {

     public static String returnCDErrorMessageByOSType(String tempPath){
        if(FindOS.isWindows()) {
            return "The system cannot find the path specified.";
        }else if(FindOS.isUnix()) {
            return "cd: "+tempPath +" No such file or directory.";
        }else{
            return "The system cannot find the path specified.";
        }
    }


    public static String getPathByOSType(String CurrDir, String cdDts) {

        if(FindOS.isWindows()) {
            if(cdDts.indexOf(":\\") >= 0) {
                 return cdDts;
            }else {
                 return  CurrDir + "\\" + cdDts;    
            }

        }else if(FindOS.isUnix()) {
           if(cdDts.indexOf("/") == 0) {
               return cdDts;
           }else {
              return CurrDir + "/" + cdDts;
           }
        }

      return "";
    }


     public static StringTokenizer breakStringBasedOnOS(String cdcommand)  {
        StringTokenizer stk = null;

        if(FindOS.isWindows()) {
             stk = new StringTokenizer(cdcommand, getSlashBasedOnOSType());
        }else if(FindOS.isUnix()) {
             stk = new StringTokenizer(cdcommand, getSlashBasedOnOSType());
        }else if(FindOS.isMac()){
           //todo
        }else{
          stk = new StringTokenizer(cdcommand, getSlashBasedOnOSType());
        }

        return stk;
    }

    /** There will be paths as tokens we have to frame a full path with slashes. This happens based on OS TYPE
     *
     * @param listPath
     * @return
     */
    public static String buildPathBasedOnOSType(List<String> listPath) {

        String path = null;
		for(String str : listPath){
			if(path == null) {
				if(FindOS.isWindows()) {
					path = str;
				}else{
				  path = getSlashBasedOnOSType() + str;
				}
			}else{
				path = path+ getSlashBasedOnOSType() + str;
			}
		}

        return path;

    }


    /** This method return slash based on OS Type
     *
     * @return
     */
    public static String getSlashBasedOnOSType() {
        if(FindOS.isWindows()) {
            return "\\";
        }else if(FindOS.isUnix()) {
            return "/";
        }else {
            return "\\";
        }
    }


}
