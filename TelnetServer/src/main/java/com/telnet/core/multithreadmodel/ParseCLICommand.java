package com.telnet.core.multithreadmodel;

import com.telnet.core.interfaces.OSType;
import com.telnet.core.util.FindOS;
import com.telnet.core.util.CLICommands;
import com.telnet.core.util.TelnetUtil;

import java.io.PrintWriter;
import java.io.InputStream;
import java.util.StringTokenizer;

/**
 * Created by IntelliJ IDEA.
 * User: nareshk
 * Date: Dec 29, 2012
 * Time: 8:59:08 PM
 * To change this template use File | Settings | File Templates.
 */
public class ParseCLICommand {
    private String cmd;
    private PrintWriter out;
    private InputStream is;

    public ParseCLICommand(String cmd, PrintWriter out, InputStream is) {
        this.cmd = cmd;
        this.out = out;
        this.is = is;
    }


    public void parseInputCommand(String CURRENT_DIRECTORY) {

        /*if(CURRENT_DIRECTORY == null || CURRENT_DIRECTORY.equals("")) {
           CURRENT_DIRECTORY =  TelnetUtil.getUserHomeDirectory();
        }*/

		OSType ostype = FindOS.findOSUnderneath();   //using factory pattern concrete object will be returned based on OS Type

		StringTokenizer stk = new StringTokenizer(cmd, " ");

		cmd = stk.nextToken();

		CLICommands.WindowsCommands wcmd = CLICommands.WindowsCommands
				.value(cmd);

		if (wcmd != null) {

			switch (wcmd) {
            case LS:
                String lsParams = null;
				try{
					lsParams = stk.nextToken();
				}catch(Exception ex){lsParams = null;}
                String list = ostype.listCurrentDirecotryDetails(lsParams, CURRENT_DIRECTORY);
                System.out.println("ls -l "+ list);
                out.println(list);
                break;
			case PWD:
				//String outputData = ostype.getPresentWorkingDirectory();
				out.println(CURRENT_DIRECTORY);
				break;
			case DIR:
				String filesList = ostype.listCurrentDirecotryDetails(cmd, CURRENT_DIRECTORY);
				out.println(filesList);
				break;
			case CD:
				String stringAfterCD = null;
				try{
					stringAfterCD = stk.nextToken();
				}catch(Exception ex){stringAfterCD = null;}

				String finalPath = ostype.executeCDCommand(stringAfterCD, CURRENT_DIRECTORY);
				if(TelnetUtil.validateIfPathExistForDirectory(finalPath)) {
					CURRENT_DIRECTORY = finalPath;
				}
                System.out.println("cd command "+ finalPath);
				out.println(finalPath);				
				break;
			case MKDIR:
				String stringAfterMKDIR = null;
				try{
					stringAfterMKDIR = stk.nextToken();
				}catch(Exception ex){stringAfterMKDIR = null;}

				String outfile = ostype.executeMKDIRCommand(stringAfterMKDIR, CURRENT_DIRECTORY);
				if(outfile != null) {
				   out.println("");
				}else{

				}
			default:
				break;

			}
		}else {
			out.print("Unable to find the command");
		}

	}
}
